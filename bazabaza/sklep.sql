-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 25 Wrz 2019, 12:44
-- Wersja serwera: 10.1.28-MariaDB
-- Wersja PHP: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `sklep`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `dostawca`
--

CREATE TABLE `dostawca` (
  `ID` int(10) NOT NULL,
  `nazwa` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `adres` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `telefon` varchar(32) COLLATE utf8_polish_ci NOT NULL,
  `NIP` varchar(16) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `dostawca`
--

INSERT INTO `dostawca` (`ID`, `nazwa`, `adres`, `telefon`, `NIP`) VALUES
(1, 'Mareczek', 'Jaracza 12 Szczecin', '827634102', '1723846280'),
(2, 'Łukaszek', 'Niepokalaniej 36 Bielice', '623245102', '2323234180'),
(3, 'Zenek', 'Klonowa 45/2 Pyrzyce', '231413202', '8456392280');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `magazyn`
--

CREATE TABLE `magazyn` (
  `ID` int(10) NOT NULL,
  `alias` varchar(128) COLLATE utf8_polish_ci NOT NULL,
  `ulica` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `miasto` varchar(128) COLLATE utf8_polish_ci NOT NULL,
  `kod` varchar(16) COLLATE utf8_polish_ci NOT NULL,
  `kraj` varchar(128) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `magazyn`
--

INSERT INTO `magazyn` (`ID`, `alias`, `ulica`, `miasto`, `kod`, `kraj`) VALUES
(1, 'BigDigStorage', 'Struga 162', 'Szczecin', '74-200', 'Polska'),
(2, '\r\nБольшой Склад', 'Иоанн Павел II', 'Moscov', '233-500', 'Rosja'),
(3, 'Magazin', 'Barnima 64', 'Berlin', '54-100', 'Niemcy');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `produkt`
--

CREATE TABLE `produkt` (
  `ID` int(10) NOT NULL,
  `nazwa` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `opis` text COLLATE utf8_polish_ci NOT NULL,
  `kod_produktu` varchar(16) COLLATE utf8_polish_ci NOT NULL,
  `cena` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `produkt`
--

INSERT INTO `produkt` (`ID`, `nazwa`, `opis`, `kod_produktu`, `cena`) VALUES
(1, 'Geberit 100sx', 'Muszla klozetowa', '1', 150),
(2, 'Klapklap 5000', 'Klapa', '2', 50),
(3, 'Geberit 5P', 'Spłóczka klozetowa', '3', 120);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `stany`
--

CREATE TABLE `stany` (
  `ID` int(10) NOT NULL,
  `ID_produktu` int(10) NOT NULL,
  `ID_magazynu` int(10) NOT NULL,
  `ilosc` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `stany`
--

INSERT INTO `stany` (`ID`, `ID_produktu`, `ID_magazynu`, `ilosc`) VALUES
(11, 3, 1, 12),
(12, 3, 1, 12),
(13, 2, 1, 32);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `dostawca`
--
ALTER TABLE `dostawca`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `magazyn`
--
ALTER TABLE `magazyn`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `produkt`
--
ALTER TABLE `produkt`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `stany`
--
ALTER TABLE `stany`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_produktu` (`ID_produktu`),
  ADD KEY `ID_magazynu` (`ID_magazynu`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `dostawca`
--
ALTER TABLE `dostawca`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `magazyn`
--
ALTER TABLE `magazyn`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `produkt`
--
ALTER TABLE `produkt`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `stany`
--
ALTER TABLE `stany`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `stany`
--
ALTER TABLE `stany`
  ADD CONSTRAINT `stany_ibfk_1` FOREIGN KEY (`ID_produktu`) REFERENCES `produkt` (`ID`),
  ADD CONSTRAINT `stany_ibfk_2` FOREIGN KEY (`ID_magazynu`) REFERENCES `magazyn` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
