-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 25 Wrz 2019, 12:09
-- Wersja serwera: 10.4.6-MariaDB
-- Wersja PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `taka sobie`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `dostawca`
--

CREATE TABLE `dostawca` (
  `ID` int(10) NOT NULL,
  `nazwa` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `adres` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `telefon` varchar(32) COLLATE utf8_polish_ci NOT NULL,
  `NIP` varchar(16) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `dostawca`
--

INSERT INTO `dostawca` (`ID`, `nazwa`, `adres`, `telefon`, `NIP`) VALUES
(1, 'Diler Dziewiona', 'Pyrzyce 9', '909909909', '1212121212'),
(2, 'Drwal Puchatek', 'Bielice 12', '367821344', '9812983490'),
(3, 'Aptekarz Mati', 'Lipiany 10', '789763232', '8781230983'),
(4, 'Kryminał Rychlik', 'Skrzynka 6', '712843921', '8787878787');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `magazyn`
--

CREATE TABLE `magazyn` (
  `ID` int(10) NOT NULL,
  `alias` varchar(128) COLLATE utf8_polish_ci NOT NULL,
  `ulica` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `miasto` varchar(128) COLLATE utf8_polish_ci NOT NULL,
  `kod` varchar(16) COLLATE utf8_polish_ci NOT NULL,
  `kraj` varchar(128) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `magazyn`
--

INSERT INTO `magazyn` (`ID`, `alias`, `ulica`, `miasto`, `kod`, `kraj`) VALUES
(1, '', 'Rogatki', 'Pyrzyce', '99', 'Polska'),
(2, '', 'Dąbska', 'Bielice', '45', 'Polska'),
(3, '', 'Cyka', 'Uszakowo', '66', 'Rosja'),
(4, '', 'Pycka', 'Japlonec nad Izerolu', '11', 'Czechy');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `produkt`
--

CREATE TABLE `produkt` (
  `ID` int(10) NOT NULL,
  `nazwa` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `opis` text COLLATE utf8_polish_ci NOT NULL,
  `kod_produktu` varchar(16) COLLATE utf8_polish_ci NOT NULL,
  `cena` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `produkt`
--

INSERT INTO `produkt` (`ID`, `nazwa`, `opis`, `kod_produktu`, `cena`) VALUES
(1, 'Smirnof', 'Rosyjska Woda Ognista prosto od Rosyjskiego Kryminalisty, który pędzi ją w domu z nieznanego przepisu', '11', 70),
(2, 'Koligurat', 'Pasek na Babe coby grzeczna była', '12', 32),
(3, 'Pytka', 'Czeskiego pochodzenia kiełbasa we flaku z kozy. Domowej roboty.', '13', 20),
(4, 'Vodni Melon', 'Zielony Arbuz w żółte prążki, zaprawiany wodą ognistą pochodzenia Czeskiego', '14', 15),
(5, 'Biały Proszek', 'Nie wiadomo czy to mąka, cukier puder czy coś innego. Wiadomo tylko że ciasta z nim dają kopa', '15', 90),
(6, 'Tabletki Teleportacji', 'Po zażyciu pojawiasz się w magicznej krainie ze smokami, dinozaurami i innymi grzybami', '16', 56),
(7, 'Drwa cienki', 'Cienko porąbane drzewo prosto z Stumilowego Lasu', '17', 77),
(8, 'Ciosany Kołek', 'ociosany i wypolerowany kołek, prosto od Drwala ze Stumilowego Lasu', '18', 100);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `stany`
--

CREATE TABLE `stany` (
  `ID` int(10) NOT NULL,
  `ilość` int(10) NOT NULL,
  `ID_magazynu` int(10) NOT NULL,
  `ID_produktu` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `stany`
--

INSERT INTO `stany` (`ID`, `ilość`, `ID_magazynu`, `ID_produktu`) VALUES
(1, 300, 1, 5),
(2, 200, 1, 6),
(3, 800, 4, 3),
(4, 600, 4, 4),
(5, 999, 3, 2),
(6, 823, 3, 1),
(7, 100, 2, 8),
(8, 100, 2, 7);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `dostawca`
--
ALTER TABLE `dostawca`
  ADD PRIMARY KEY (`ID`);

--
-- Indeksy dla tabeli `magazyn`
--
ALTER TABLE `magazyn`
  ADD PRIMARY KEY (`ID`);

--
-- Indeksy dla tabeli `produkt`
--
ALTER TABLE `produkt`
  ADD PRIMARY KEY (`ID`);

--
-- Indeksy dla tabeli `stany`
--
ALTER TABLE `stany`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `dostawca`
--
ALTER TABLE `dostawca`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `magazyn`
--
ALTER TABLE `magazyn`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT dla tabeli `produkt`
--
ALTER TABLE `produkt`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT dla tabeli `stany`
--
ALTER TABLE `stany`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
