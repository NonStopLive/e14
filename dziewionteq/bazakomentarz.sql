-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 22 Paź 2019, 12:29
-- Wersja serwera: 10.4.6-MariaDB
-- Wersja PHP: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `bazakomentarz`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `komentarze`
--

CREATE TABLE `komentarze` (
  `ID` int(11) NOT NULL,
  `Autor` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `Tresc` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `komentarze`
--

INSERT INTO `komentarze` (`ID`, `Autor`, `Tresc`) VALUES
(143, 'DziewioNa', 'TEST'),
(144, 'DziewioNa', 'TEST'),
(145, 'DziewioNa', 'TEST'),
(146, 'Anonim', 'Mateusz ty gÅ‚upi ciulu wszystko dziaÅ‚a nie czepiaj siÄ™ :( '),
(147, 'Anonim', 'Mateusz ty gÅ‚upi ciulu wszystko dziaÅ‚a nie czepiaj siÄ™ :( '),
(148, 'Anonim', 'Mateusz ty gÅ‚upi ciulu wszystko dziaÅ‚a nie czepiaj siÄ™ :( '),
(149, 'Anonim', 'Mateusz ty gÅ‚upi ciulu wszystko dziaÅ‚a nie czepiaj siÄ™ :( '),
(150, 'Anonim', 'Mateusz ty gÅ‚upi ciulu wszystko dziaÅ‚a nie czepiaj siÄ™ :( '),
(151, 'Anonim', 'Mateusz ty gÅ‚upi ciulu wszystko dziaÅ‚a nie czepiaj siÄ™ :( '),
(152, 'Anonim', 'Mateusz ty gÅ‚upi ciulu wszystko dziaÅ‚a nie czepiaj siÄ™ :( '),
(153, 'Anonim', 'Mateusz ty gÅ‚upi ciulu wszystko dziaÅ‚a nie czepiaj siÄ™ :( '),
(154, 'Anonim', 'Mateusz ty gÅ‚upi ciulu wszystko dziaÅ‚a nie czepiaj siÄ™ :( '),
(155, 'Anonim', 'Mateusz ty gÅ‚upi ciulu wszystko dziaÅ‚a nie czepiaj siÄ™ :( '),
(156, 'Anonim', 'Mateusz ty gÅ‚upi ciulu wszystko dziaÅ‚a nie czepiaj siÄ™ :( '),
(157, 'Anonim', 'Mateusz ty gÅ‚upi ciulu wszystko dziaÅ‚a nie czepiaj siÄ™ :( '),
(158, 'Anonim', 'Mateusz ty gÅ‚upi ciulu wszystko dziaÅ‚a nie czepiaj siÄ™ :( ');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `komentarze`
--
ALTER TABLE `komentarze`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `komentarze`
--
ALTER TABLE `komentarze`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=159;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
