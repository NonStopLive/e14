-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 25 Wrz 2019, 10:02
-- Wersja serwera: 10.4.6-MariaDB
-- Wersja PHP: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `dziewiona`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `dostawca`
--

CREATE TABLE `dostawca` (
  `ID` int(10) NOT NULL,
  `Nazwa` varchar(259) COLLATE utf8mb4_polish_ci NOT NULL,
  `Adres` varchar(255) COLLATE utf8mb4_polish_ci NOT NULL,
  `Telefon` varchar(32) COLLATE utf8mb4_polish_ci NOT NULL,
  `NIP` varchar(16) COLLATE utf8mb4_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

--
-- Zrzut danych tabeli `dostawca`
--

INSERT INTO `dostawca` (`ID`, `Nazwa`, `Adres`, `Telefon`, `NIP`) VALUES
(1, 'Dostawca Fabian', 'Dostawianki 3a/3', '555666888', '12-25-26-55'),
(2, 'Michau kierowca ', 'Skrzynka 13/4b', '332255447', '14-11-55-77-88'),
(3, 'Hubert pakownik', 'pakowianki 4', '223558447', '25-66-69-67');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `magazyn`
--

CREATE TABLE `magazyn` (
  `ID` int(10) NOT NULL,
  `Alias` varchar(128) COLLATE utf8mb4_polish_ci NOT NULL,
  `Ulica` varchar(255) COLLATE utf8mb4_polish_ci NOT NULL,
  `Miasto` varchar(128) COLLATE utf8mb4_polish_ci NOT NULL,
  `Kod` varchar(16) COLLATE utf8mb4_polish_ci NOT NULL,
  `Kraj` varchar(128) COLLATE utf8mb4_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

--
-- Zrzut danych tabeli `magazyn`
--

INSERT INTO `magazyn` (`ID`, `Alias`, `Ulica`, `Miasto`, `Kod`, `Kraj`) VALUES
(1, '', 'Magazynowa 3', 'Magazynki', '74-999', 'Polska'),
(2, '', 'Francisława 14', 'Franciszki', '69-999', 'Polska'),
(3, '', 'Michalinki 9', 'Michałowice', '45-856', 'Polska'),
(4, '', 'Nikodemki 3/3', 'Nikodemice', '45-885', 'Ukraina');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `produkt`
--

CREATE TABLE `produkt` (
  `ID` int(10) NOT NULL,
  `Nazwa` varchar(255) COLLATE utf8mb4_polish_ci NOT NULL,
  `Opis` text COLLATE utf8mb4_polish_ci NOT NULL,
  `Kod-produktu` varchar(16) COLLATE utf8mb4_polish_ci NOT NULL,
  `Cena` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

--
-- Zrzut danych tabeli `produkt`
--

INSERT INTO `produkt` (`ID`, `Nazwa`, `Opis`, `Kod-produktu`, `Cena`) VALUES
(1, 'Vapepen 22', 'Smok Vape pen 22\r\nBardzo fajny skromny epecik ', '22', 100),
(2, 'Smok Stick V9 ', 'Bardzo przyjemny i fajny w uzytkowaniu epecik', '9', 169.99),
(3, 'Smok Prince', 'Szybki epet dobre chmury bierz\r\npóki ciepły', '16', 140),
(4, 'Epet od Boga', 'Najzajefajniejszy Epet w Polsce albo na całym świecie! ', '5', 9999),
(5, 'Ijust 3', 'Taki fajny pierdzawek do kopcenia w domu pracy samochodzie', '6', 129),
(6, 'Ijust 21700', 'Szybki fajny ladny', '2', 199),
(7, 'Eleaf Pico', 'Fajny epet z akumulatorkiem', '4', 80),
(8, 'Ijust GEN', 'nowa generacja popularnego epapierosa jakim jest Ijust ', '8', 179),
(9, 'Tesla 200W', 'Najnowocześniejszy epapieros z unikalnym wyglądem potrafiący puścić chmurę że zadymi całe miasto', '17', 350),
(10, 'Tesla WYE 200W Klin V2', 'Najlepszy z najlepszych epecik prosto od Użytkownika najlepszej sieci epapieroskiej na Polskim Rynku, nie znajdziecie lepszego Epapierosa od tego! ', '65', 500);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `stany`
--

CREATE TABLE `stany` (
  `ID` int(10) NOT NULL,
  `IDProduktu` int(10) NOT NULL,
  `IDMagazynu` int(10) NOT NULL,
  `Ilość` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

--
-- Zrzut danych tabeli `stany`
--

INSERT INTO `stany` (`ID`, `IDProduktu`, `IDMagazynu`, `Ilość`) VALUES
(1, 1, 1, 15),
(2, 2, 2, 3),
(3, 3, 3, 45),
(4, 4, 4, 1),
(5, 5, 2, 12),
(6, 6, 1, 24),
(7, 7, 4, 1),
(8, 8, 1, 15),
(9, 9, 3, 4),
(10, 10, 1, 1);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `dostawca`
--
ALTER TABLE `dostawca`
  ADD PRIMARY KEY (`ID`);

--
-- Indeksy dla tabeli `magazyn`
--
ALTER TABLE `magazyn`
  ADD PRIMARY KEY (`ID`);

--
-- Indeksy dla tabeli `produkt`
--
ALTER TABLE `produkt`
  ADD PRIMARY KEY (`ID`);

--
-- Indeksy dla tabeli `stany`
--
ALTER TABLE `stany`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `IDProduktu` (`IDProduktu`),
  ADD KEY `IDMagazynu` (`IDMagazynu`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `dostawca`
--
ALTER TABLE `dostawca`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `magazyn`
--
ALTER TABLE `magazyn`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `produkt`
--
ALTER TABLE `produkt`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT dla tabeli `stany`
--
ALTER TABLE `stany`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
